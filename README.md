# Submission README #
## Author: Edgars Eglitis (1353184) ##

This coursework was fully developed on DICE and thus it is expected to cause no problems upon attempting to compile and run it. This can be accomplished as follows:

* running "make" in the root directory should produce an executable file called "simulation".
* afterwards, running "./simulation [input file name]" in the root directory should execute the simulation.

This application does not include the following functionality:

* rebuilding the shortest paths
* support for experiments

All source code is located in the "src" directory.
Several input tests were also created, both correct and incorrect. These are located in the "tests" directory.

## Important Notes ##

Parser:

* the parser is based on separating tokens with the whitespace delimiter, and may therefore produce incorrect results if tokens are delimited with tabs
* lines consisting solely of spaces and/or tabs may not be recognized correctly

Simulation:

* a bus does not receive any requests while it is driving, boarding or disembarking passengers
* a bus driving from stop A to B does not disembark passengers at stops that the bus must pass by in order to reach stop B. This is due to the lack of support for rebuilding the shortest paths.
* the simulation may produce incorrect output if several buses attempt to execute the same event at the exact same time. For this purpose, events consist of departure, arrival, and boarding/disembarkation.
