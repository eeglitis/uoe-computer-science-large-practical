################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/busesandrequests.c \
../src/events.c \
../src/floydwarshall.c \
../src/helpers.c \
../src/parser.c \
../src/postloop.c \
../src/simulation.c 

OBJS += \
./src/busesandrequests.o \
./src/events.o \
./src/floydwarshall.o \
./src/helpers.o \
./src/parser.o \
./src/postloop.o \
./src/simulation.o 

C_DEPS += \
./src/busesandrequests.d \
./src/events.d \
./src/floydwarshall.d \
./src/helpers.d \
./src/parser.d \
./src/postloop.d \
./src/simulation.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


