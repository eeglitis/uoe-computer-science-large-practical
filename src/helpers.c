#include "simulation.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <float.h>

#define D2H 24 // days to hours
#define H2M 60 // hours to minutes
#define M2S 60 // minutes to seconds

// reference for toFloat and toInt:
// http://stackoverflow.com/questions/78474/determine-if-a-string-is-an-integer-or-a-float-in-ansi-c

// converts the non-whitespace part of a string to a float representation. used in parser.c
float toFloat(char* str) {
	char* p;
	errno = 0;
	double result = strtod(str, &p);
	if ((errno != 0) || (str == p) || ((*p != 0) && (*p != '\n') && (*p != '\t') && (*p != ' ')) || (result < FLT_MIN) || (result > FLT_MAX)) {
		printf("Error: expected float, got '%s'!\n", str);
		exit(EXIT_FAILURE);
	}
	return (float) result;
}

// converts the non-whitespace part of a string to a positive int representation. used in parser.c for the non-matrix integer values (as they must be strictly positive)
int toInt(char* str) {
	char* p;
	errno = 0;
	long result = strtol(str, &p, 0);
	if ((errno != 0) || (str == p) || ((*p != 0) && (*p != '\n') && (*p != '\t') && (*p != ' ')) || (result < 1) || (result > INT_MAX)) {
		printf("Error: expected positive integer, got '%s'!\n", str);
		exit(EXIT_FAILURE);
	}
	return (int) result;
}

// returns a random number in the provided range. used in busesandrequests.c for request pickup time, and simulation.c for request arrival time
float randomize(int low, int high) {
	double val = (double) rand()/RAND_MAX;
	return (float) val*(high-low)+low;
}

// converts the time in seconds to a formatted string representation (DD:HH:MM:SS)
char* ftime(char* array, int time) {
	int days, hours, minutes, seconds;
	days = (time - (time % (D2H*H2M*M2S)))/(D2H*H2M*M2S);
	array[0] = days/10 + '0';
	array[1] = days%10 + '0';
	array[2] = ':';
	time = time % (D2H*H2M*M2S);
	hours = (time - (time % (H2M*M2S)))/(H2M*M2S);
	array[3] = hours/10 + '0';
	array[4] = hours%10 + '0';
	array[5] = ':';
	time = time % (H2M*M2S);
	minutes = (time - (time % M2S))/M2S;
	array[6] = minutes/10 + '0';
	array[7] = minutes%10 + '0';
	array[8] = ':';
	seconds = time % M2S;
	array[9] = seconds/10 + '0';
	array[10] = seconds%10 + '0';
	array[11] = '\0';
	return array;
}

// returns the sum of the passengers and pending requests of a bus. used in the assignRequest function in simulation.c
int totalSpace(bus b, int size) {
	int k;
	int sum = 0;
	for (k = 0; k<size; k++) {
		if (b.requests[k].pickupTime != 0) sum++;
		if (b.passengers[k].pickupTime != 0) sum++;
	}
	return sum;
}

// increments the waiting time of all passengers of a bus if it has waited at a stop
bus addWait(bus b, int curTime) {
	if (b.waitingStart<INT_MAX) {
		int r;
		for (r = 0; r<b.maxCapacity; r++) {
			if (b.passengers[r].pickupTime>0) {
				b.passengers[r].waitTime = b.passengers[r].waitTime + (curTime-b.waitingStart);
			}
		}
	}
	b.waitingStart = INT_MAX;
	return b;
}
