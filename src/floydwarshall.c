#include "simulation.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#define M2S 60 // minutes to seconds

int r, c, k, N;
int*** mat;

// returns a 2-element array, the first of which contains the shortest distances matrix, while the other contains the next hop matrix
int*** shortestPaths(parameters p) {
	N = p.noStops;

	// allocate space
	mat = (int***) malloc(2*sizeof(int**));
	mat[0] = (int**) malloc(N*sizeof(int*)); // shortest paths
	mat[1] = (int**) malloc(N*sizeof(int*)); // next hops
	for (r = 0; r<N; r++) {
		mat[0][r] = (int*) malloc(N*sizeof(int));
		mat[1][r] = (int*) malloc(N*sizeof(int));
	}

	// fill in base distances, put reasonably large value if -1
	for (r = 0; r<N; r++) {
		for (c = 0; c<N; c++) {
			if (p.matrix[r][c] == -1) {
				mat[0][r][c] = (INT_MAX-1)/2; // prevents overflowing when adding in main loop
			} else mat[0][r][c] = p.matrix[r][c]*M2S;
			mat[1][r][c] = c;
		}
	}

	// main loop
	for (k = 0; k<N; k++) {
		for (r = 0; r<N; r++) {
			for (c = 0; c<N; c++) {
				if (mat[0][r][c] > mat[0][r][k] + mat[0][k][c]) {
					mat[0][r][c] = mat[0][r][k] + mat[0][k][c];
					mat[1][r][c] = mat[1][r][k];
				}
			}
		}
	}

	// can now free input matrix
	for (r = 0; r<N; r++) {
		free(p.matrix[r]);
	}
	free(p.matrix);
	return mat;
}
