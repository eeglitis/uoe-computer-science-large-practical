#include "simulation.h"
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define IDLE 0		// at stop, no requests, no passengers
#define LEAVING 1	// at stop, pending request(s)
#define MOVING 2	// moving from stop to stop
#define ATSTOP 3	// boarding or disembarking
#define C 0	// flag to update current time in boarding function
#define D 1	// flag to update departure time in boarding function

// event of a bus arriving at a stop
bus arrive(bus theBus, int curTime, int boardingTime, char* outTime) {
	if ((theBus.requests[0].pickupTime>0) && (theBus.requests[0].schTime==curTime)) { // arrival due to a request at this stop
		if (theBus.curStop == theBus.requests[0].startStop) { // bus was already present at this stop, no need to print anything
			theBus.state = ATSTOP;
			theBus.nextArrival = INT_MAX;
			theBus.nextBoardCheck = curTime+boardingTime;
			return theBus;
		}
		theBus.curStop = theBus.requests[0].startStop;
	} else if ((theBus.passengers[0].pickupTime>0) && (theBus.passengers[0].arrTime==curTime)) { // arrival due to a passenger disembarking at this stop
		theBus.curStop = theBus.passengers[0].endStop;
	}
	theBus.state = ATSTOP;
	theBus.nextArrival = INT_MAX;
	theBus.nextBoardCheck = curTime+boardingTime;
	printf("%s -> minibus %d arrived at stop %d\n", ftime(outTime, curTime), theBus.id, theBus.curStop);
	return theBus;
}

// event of a bus departing from a stop
bus depart(bus theBus, int curTime, int bTime, char* outTime, int*** mat) {
	int curStop = theBus.curStop;
	request firstReq = theBus.requests[0];
	request firstPass = theBus.passengers[0];
	theBus.state = MOVING;

	// first check if there are any pending requests, as those will take priority
	if (firstReq.pickupTime>0) {
		theBus.nextArrival = firstReq.schTime;

		// check whether the first passenger can be dropped off before arriving for the first request
		if ((firstPass.pickupTime>0) && (curTime+mat[0][curStop][firstPass.endStop]+bTime+mat[0][firstPass.endStop][firstReq.startStop]<=firstReq.schTime)) {
			firstPass.arrTime = curTime+mat[0][curStop][firstPass.endStop];
			theBus.passengers[0] = firstPass;
			theBus.nextArrival = theBus.passengers[0].arrTime;
			printf("%s -> minibus %d left stop %d\n", ftime(outTime, curTime), theBus.id, curStop);
			return theBus;
		}

		// not enough time to deliver a passenger - check if the bus can wait some time before leaving
		if (firstReq.schTime>curTime+mat[0][curStop][firstReq.startStop]) {
			theBus.state = LEAVING;
			theBus.waitingStart = curTime;
			return theBus;
		}

		// not enough time to wait - bus departs immediately
		theBus = addWait(theBus, curTime); // update the waiting times of any passengers on board
		if (firstReq.startStop == curStop) return theBus; // next request is at the same stop, no need to print departing statements
		printf("%s -> minibus %d left stop %d\n", ftime(outTime, curTime), theBus.id, curStop);

	// no pending requests - departing for the disembarking stop of the first passenger
	} else if (firstPass.pickupTime>0) {
		firstPass.arrTime = curTime+mat[0][curStop][firstPass.endStop];
		theBus.passengers[0] = firstPass;
		theBus.nextArrival = firstPass.arrTime;
		printf("%s -> minibus %d left stop %d\n", ftime(outTime, curTime), theBus.id, curStop);

	// no passengers and no requests, bus idle
	} else {
		theBus.state = IDLE;
		theBus.nextArrival = INT_MAX;
	}
	return theBus;
}

// finds the closest departure time for a bus
int depTime(int i, bus* buses, int** paths) {
	int curDepTime;
	if ((buses[i].requests[0].pickupTime>0) && (buses[i].state == LEAVING)) { // closest departure time is for a request
		curDepTime = buses[i].nextArrival - paths[buses[i].curStop][buses[i].requests[0].startStop];
	} else if ((buses[i].passengers[0].pickupTime>0) && (buses[i].state == LEAVING)) { // no requests - closest departure time is to disembark a passenger
		curDepTime = buses[i].nextArrival - paths[buses[i].curStop][buses[i].passengers[0].endStop];
	} else { // no passengers and no requests, idle
		curDepTime = INT_MAX;
	}
	return curDepTime;
}

// event of a bus boarding and/or disembarking passengers
bus boarding(bus theBus, parameters p, int curTime, int boardingTime, char* outTime, int*** mat) {
	int i, j;
	int foundOne = 0;

	// check disembarking first
	for (i = 0; i<p.busCapacity; i++) {
		do { // repeats checking at the same passenger index, in case passengers in consecutive slots want to disembark at the same stop
			if ((theBus.passengers[i].endStop == theBus.curStop) && (theBus.passengers[i].pickupTime>0)) {

				if ((theBus.requests[1].pickupTime > 0) 			// have at least 2 requests
				&& (theBus.requests[0].startStop == theBus.curStop) // first request is at this stop
				&& (curTime+2*p.boardingTime>theBus.requests[1].schTime-mat[0][theBus.curStop][theBus.requests[1].startStop])) {
					// time required to disembark this passenger in addition to boarding the passenger from first request will violate the scheduled time for next request
					break;
				}

				if ((theBus.requests[0].pickupTime > 0) 			// have at least one request
				&& (theBus.requests[0].startStop != theBus.curStop) // said request must be at a different stop (if the request is at this stop and it is the only request,
																	// the passenger can wait, but if there are multiple total requests, refer to previous if statement)
				&& (foundOne)										// already disembarked a passenger but have another (curTime already updated)
				&& (curTime+p.boardingTime>theBus.requests[0].schTime-mat[0][theBus.curStop][theBus.requests[0].startStop])) {
					// time required to disembark another passenger will violate the scheduled time for the closest request
					break;
				} // if someone else is to disembark at this stop, and there is enough time to do this, next time the loop is executed normally

				if (foundOne) return theBus; // at least 2 passengers disembarking, implying a separate event in p.boardingTime seconds
				foundOne = 1;
				curTime = updateTime(boardingTime, C);
				updateTripStats(curTime-theBus.passengers[i].boardTime, theBus.passengers[i].waitTime, mat[0][theBus.passengers[i].startStop][theBus.passengers[i].endStop]);
				updateTripEfficiency((curTime-theBus.occChangeTime)*theBus.capacity);
				theBus.occChangeTime = curTime;
				theBus.nextBoardCheck = curTime+p.boardingTime;
				theBus.capacity--;
				printf("%s -> minibus %d disembarked passenger at stop %d\n", ftime(outTime, curTime), theBus.id, theBus.curStop);
				printf("%s -> minibus %d occupancy became %d\n", ftime(outTime, curTime), theBus.id, theBus.capacity);

				// update positions of passengers
				for (j = i; j<p.busCapacity-1; j++) {
					theBus.passengers[j] = theBus.passengers[j+1];
				}
				theBus.passengers[p.busCapacity-1].pickupTime = 0; // make sure the last spot is freed

			} else if (foundOne) break;
		} while (foundOne);
	}

	// everyone disembarked, check if someone wants to board
	for (i = 0; i<p.busCapacity; i++) {
		do { // repeats checking at the same request index, in case passengers in consecutive request slots want to board at the same stop
			if ((theBus.requests[i].startStop == theBus.curStop) && (theBus.requests[i].schTime<=curTime) && (theBus.requests[i].pickupTime>0)) { // someone boarding at this stop
				if (foundOne) return theBus;  // at least 2 passengers boarding, implying a separate event in p.boardingTime seconds
				foundOne = 1;
				curTime = updateTime(boardingTime, C);
				theBus.requests[i].boardTime = curTime;
				updateTripEfficiency((curTime-theBus.occChangeTime)*theBus.capacity);
				theBus.occChangeTime = curTime;
				theBus.nextBoardCheck = curTime+p.boardingTime;
				theBus.capacity++;
				printf("%s -> minibus %d boarded passenger at stop %d\n", ftime(outTime, curTime), theBus.id, theBus.curStop);
				printf("%s -> minibus %d occupancy became %d\n", ftime(outTime, curTime), theBus.id, theBus.capacity);

				// add passenger
				for (j = 0; j<p.busCapacity; j++) {
					if (theBus.passengers[j].pickupTime == 0) {
						theBus.passengers[j] = theBus.requests[i];
						break;
					}
				}

				// remove newly fulfilled request from request array
				for (j = i; j<p.busCapacity-1; j++) {
					theBus.requests[j] = theBus.requests[j+1];
				}
				theBus.requests[p.busCapacity-1].pickupTime = 0; // make sure the last spot is freed

			} else if (foundOne) break;
		} while (foundOne);
	}

	// everyone who needed has boarded and disembarked. change bus state and look at next request
	theBus.nextBoardCheck = INT_MAX;
	theBus = depart(theBus, curTime,  p.boardingTime, outTime, mat);
	updateTime(INT_MAX, D);
	return theBus;
}
