#include "simulation.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

parameters p;
int i,j;
char* ind;

// create a matrix with the size provided in the input file
void makeMatrix() {
	p.matrix = (int**) malloc(p.noStops*sizeof(int*));
	for (i = 0; i<p.noStops; i++) {
		p.matrix[i] = (int*) malloc(p.noStops*sizeof(int));
	}
}

// gets the first non-space element and returns it if it is valid
int getValue(char* line) {
	char* nonSpace;
	int k;
	for (k = 0; k<USHRT_MAX; k++) {
		if ((line[k] == NULL) || (line[k] == '\n')) return SHRT_MIN; // no non-space elements left
		if ((line[k] != ' ') && (line[k] != '\t')) { // found a non-space element
			nonSpace = &line[k];
			int result = (int) strtol(nonSpace, &ind, 0);
			if ((result == 0) && (nonSpace[0] != '0')) { // found non-number
				printf("Error: Wrong element '%s'!\n", nonSpace);
				exit(EXIT_FAILURE);
			}
			return result;
		}
	}
	// a line is unlikely to have solely 65,535 whitespace elements, so the full loop is almost never going to be executed
	// nevertheless, a return statement is required
	return SHRT_MIN;
}

void checkForExtra(char* line, int distance) {
	char* token3;
	token3 = strtok(NULL, " ");
	if (token3 != NULL) {
		distance = getValue(token3);
		if (distance!=SHRT_MIN) {
			printf("Error: Unexpected value '%d'!\n", distance);
			exit(EXIT_FAILURE);
		}
	}
}

parameters parse(FILE* input) {
	int distance; // single integer in the matrix
	int paramcount = 0; // used to confirm we have all 9 of the required parameters
	int len = USHRT_MAX; // a line can have an undetermined number of elements, but can be at least 65535 as per specification
	char line[len];
	ind = line;
	char* token1;
	char* token2;
	p.noStops = 0; // initialization

	// first check that the file is actually present
	if (input == NULL) {
		printf("Error opening file!\n");
		exit(EXIT_FAILURE);
	}

	// iterate until the end of file
	while (fgets(line, len, input)) {
		ind = line;
		token1 = strtok(line, " ");
		token2 = strtok(NULL, " ");

		// ignore comments and empty lines
		if ((line[0] == '#') || (strcmp(token1, "\n") == 0)) {

		// found 'map', check that token2 (if present) is only whitespaces/tabs
		} else if (((strcmp(token1, "map") == 0) && (getValue(token2) == SHRT_MIN)) || (strcmp(token1, "map\n") == 0)) {

			// check that noStops has been declared beforehand
			if (p.noStops == 0) {
				printf("Error: The map keyword must be preceded by the number of stops!\n");
				exit(EXIT_FAILURE);
			}
			makeMatrix();

			// iterate through matrix rows
			for (i = 0; i<p.noStops; i++) {

				// check if there are fewer rows than needed
				if (fgets(line, len, input) == NULL) {
					printf("Error: Matrix has fewer rows than needed (expected %d, got %d)!\n", p.noStops, i);
					exit(EXIT_FAILURE);
				}
				ind = line;

				// iterate through matrix columns
				for (j = 0; j<p.noStops; j++) {
					distance = getValue(ind);

					// check again if there are fewer rows than needed; check if there are fewer columns than needed
					if (distance == SHRT_MIN) {
						if (j == 0) {
							printf("Error: Matrix has fewer rows than needed (expected %d, got %d)!\n", p.noStops, i);
							exit(EXIT_FAILURE);
						}
						printf("Error: Matrix has fewer columns than needed (expected %d, got %d)!\n", p.noStops, j);
						exit(EXIT_FAILURE);
					}

					// check for incorrect integer values
					if (((distance == 0) && (i != j))	// value 0 somewhere other than the main diagonal
					|| ((i == j) && (distance != 0))	// not value 0 on the main diagonal
					|| (distance < -1)) {				// integer below -1
						printf("Error: Incorrect matrix value '%d'!\n", distance);
						exit(EXIT_FAILURE);
					}
					p.matrix[i][j] = distance;
				}

				// check if there are more columns than needed
				distance = getValue(ind);
				if (distance != SHRT_MIN) {
					printf("Error: Matrix has more columns than needed (expected %d)!\n", p.noStops);
					exit(EXIT_FAILURE);
				}
			}
			paramcount++;

		// found "busCapacity"
		} else if (strcmp(token1,"busCapacity") == 0) {
			p.busCapacity = toInt(token2);
			if (p.busCapacity>24) {
				printf("Error: Bus capacity must be no more than 24!\n");
				exit(EXIT_FAILURE);
			}
			checkForExtra(line, distance);
			paramcount++;

		// found "boardingTime"
		} else if (strcmp(token1,"boardingTime") == 0) {
			p.boardingTime = toInt(token2);
			checkForExtra(line, distance);
			paramcount++;

		// found "requestRate"
		} else if (strcmp(token1,"requestRate") == 0) {
			p.requestRate = toFloat(token2);
			checkForExtra(line, distance);
			paramcount++;

		// found "pickupInterval"
		} else if (strcmp(token1,"pickupInterval") == 0) {
			p.pickupInterval = toFloat(token2);
			checkForExtra(line, distance);
			paramcount++;

		// found "stopTime"
		} else if (strcmp(token1,"stopTime") == 0) {
			p.stopTime = toFloat(token2);
			checkForExtra(line, distance);
			paramcount++;

		// found "maxDelay"
		} else if (strcmp(token1,"maxDelay") == 0) {
			p.maxDelay = toInt(token2);
			checkForExtra(line, distance);
			paramcount++;

		// found "noBuses"
		} else if (strcmp(token1,"noBuses") == 0) {
			p.noBuses = toInt(token2);
			checkForExtra(line, distance);
			paramcount++;

		// found "noStops"
		} else if (strcmp(token1,"noStops") == 0) {
			p.noStops = toInt(token2);
			checkForExtra(line, distance);
			paramcount++;

		// found an incorrect token - may be a number for an extra matrix row, or simply incorrect string keyword
		} else {
			if (strcmp(token1, "map") == 0) { // already verified that token2 is non-space
				printf("Error: Token 'map' is followed by invalid symbols!\n");
			} else printf("Error: Token '%s' is invalid, or matrix has more rows than needed!\n", token1);
			exit(EXIT_FAILURE);
		}
	}

	// if parameters have been repeated, or some are missing, return error
	if (paramcount != 9) {
		printf("Error: Incorrect number of parameters provided!\n");
		exit(EXIT_FAILURE);
	}
	return p;
}
