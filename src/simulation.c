#include "simulation.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define H2M 60 // hours to minutes
#define M2S 60 // minutes to seconds
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))
#define IDLE 0		// at stop, no requests, no passengers
#define LEAVING 1	// at stop, pending request(s)
#define ATSTOP 3	// boarding or disembarking
#define C 0			// flag to update current time	(boarding procedure in events.c)

char* outTime; 			// points to formatted time string
int curTime; 			// constantly updated
int meanRequestDelay; 	// mean time in seconds between requests - set once
int maxTime; 			// used to convert stopTime from hours to seconds - set once
parameters p; 			// parameter storage
request r; 				// stores the immediate request
int*** mat; 			// pointer to a 2-element array, where element 0 is the distance matrix, and element 1 is next hop matrix
bus* buses; 			// bus array

int requestTime; 	// time the next request is scheduled to come
int departureTime; 	// closest time a bus will depart
int arrivalTime; 	// closest time a bus will arrive
int boardingTime; 	// used only if a bus state = ATSTOP
int closestTime; 	// smallest of the 4 aforementioned delays

int depBusIndex; 	// index of bus with closest departure time
int arrBusIndex; 	// index of bus with closest arrival time
int boardBusIndex; 	// index of bus with closest boarding/disembarking time

// variables for statistical purposes
int tripDuration; 	// total time of all trip durations
int noTrips; 		// total number of trips completed
int occSums; 		// for 'occ' being an instantaneous occupancy of a bus, multiplied by the time period during which it stays the same, this is the sum of all 'occ', for all buses
int accReqs; 		// total number of accepted requests
int missedReqs; 	// total number of missed requests
int waitingTime; 	// total time spent waiting at a bus stop for all passengers (only for finished trips)
int deviationTime; 	// total sum of all trip deviation times from shortest path (only for finished trips)

// initializes statistical variables to 0 for safety
void initStats() {
	tripDuration = 0;
	noTrips = 0;
	occSums = 0;
	accReqs = 0;
	missedReqs = 0;
	waitingTime = 0;
	deviationTime = 0;
}

// used for average trip duration, average waiting time, average deviation time calculation
void updateTripStats(int tripLen, int waitTime, int shortestTrip) {
	tripDuration = tripDuration+tripLen;
	waitingTime = waitingTime+waitTime;
	deviationTime = deviationTime+(tripLen-shortestTrip-p.boardingTime);
	noTrips++;
}

// used for average trip efficiency
void updateTripEfficiency(int occ) {
	occSums = occSums+occ;
}

// calculates the final statistics, also accounting for the case if no trips have been completed yet
void printStats() {
	int avgTripDuration, avgWaitTime, avgTripDeviation;
	float tripEfficiency, missedReqFraction;
	if (noTrips == 0) {
		avgTripDuration = 0;
		avgWaitTime = 0;
		avgTripDeviation = 0;
	} else {
		avgTripDuration = tripDuration/noTrips;
		avgWaitTime = waitingTime/noTrips;
		avgTripDeviation = deviationTime/noTrips;
	}
	tripEfficiency = (float) occSums/(curTime*p.noBuses);
	missedReqFraction = (float) missedReqs/(accReqs+missedReqs);
	getStatistics(avgTripDuration, tripEfficiency, missedReqFraction, avgWaitTime, avgTripDeviation);
}

// used for updating curTime and departureTime, called from the boarding function in events.c
int updateTime(int newTime, int class) {
	if (class == C)	curTime = newTime;
	else departureTime = newTime;
	return newTime;
}

// determines whether a request can be satisfied, and if so, assigns it to a bus
// the function prioritizes assigning requests to buses with the least number of pending requests+passengers
void assignRequest() {
	int i,j,k;	// counters
	bus theBus; // for ease of use

	// the text about a new request must be printed regardless of whether it is satisfiable or not, so start with that
	char stT[11];
	char depT[11];
	strcpy(stT, ftime(outTime, curTime));
	strcpy(depT, ftime(outTime, r.pickupTime));
	printf("%s -> new request placed from stop %d to stop %d for departure at %s ", stT, r.startStop, r.endStop, depT);

	// start with assigning all idle buses
	for (i = 0; i<p.noBuses; i++) {
		theBus = buses[i];
		if ((theBus.state == IDLE) && (r.pickupTime+p.maxDelay*M2S >= curTime+mat[0][theBus.curStop][r.startStop])) { // can be accommodated
			theBus.nextArrival = MAX(r.pickupTime, curTime+mat[0][theBus.curStop][r.startStop]);
			r.schTime = theBus.nextArrival;
			theBus.requests[0] = r;
			theBus.state = LEAVING;
			accReqs++;
			printf("scheduled for %s\n", ftime(outTime, theBus.nextArrival));
			buses[i] = theBus;
			return;
		}
	}

	// next check those who have request(s) but are static, and can pick up the new request

	// first initialize array of possibly applicable bus IDs
	int* appBuses = (int*) malloc(p.noBuses*sizeof(int));
	for (i = 0; i<p.noBuses; i++) {
		appBuses[i] = buses[i].id;
	}

	int appBusCount = p.noBuses; 	// size of appBuses
	int futureStop; 				// a startStop for one of the bus' current requests
	int futureArrTime; 				// time the bus would arrive at futureStop
	int futureDepTime; 				// time the bus would be ready to leave futureStop for the new request
	for (i = 0; i<p.noBuses; i++) {

		// first find the bus with the least number of requests+passengers
		theBus = buses[appBuses[0]-1];
		int arrPos = 0; // the position in appBuses of theBus' id
		for (j = 1; j<appBusCount; j++) {
			if (totalSpace(theBus, p.busCapacity)>totalSpace(buses[appBuses[j]-1], p.busCapacity)) {
				theBus = buses[appBuses[j]-1];
				arrPos = j;
			}
		}

		if (totalSpace(theBus, p.busCapacity)==p.busCapacity) { // all applicable buses full, can't add request
			break;
		}

		// now check if this bus can satisfy the new request
		if ((theBus.state == LEAVING)
		&& (r.pickupTime + p.maxDelay * M2S	>= curTime + mat[0][theBus.curStop][r.startStop])) {	// check if the new request is reachable in time
			r.schTime = MAX(r.pickupTime, curTime+mat[0][theBus.curStop][r.startStop]);

			// check if the new request can be picked up before the first pending request
			if ((r.schTime+p.boardingTime+mat[0][r.startStop][theBus.requests[0].startStop]<=theBus.nextArrival)) {
				for (j = p.busCapacity-1; j>0; j--) { // update request array
					theBus.requests[j] = theBus.requests[j-1];
				}
				theBus.nextArrival = r.schTime;
				theBus.requests[0] = r;
				theBus.state = LEAVING;
				accReqs++;
				printf("scheduled for %s\n", ftime(outTime, theBus.nextArrival));
				buses[theBus.id-1] = theBus;
				free(appBuses);
				return;
			}

			// cannot make before the first request - iterate and see where can it be added
			futureArrTime = theBus.nextArrival;
			futureStop = theBus.requests[0].startStop;
			for (j = 1; j<p.busCapacity; j++) {
				futureDepTime = futureArrTime+p.boardingTime;
				r.schTime = MAX(r.pickupTime, futureDepTime+mat[0][futureStop][r.startStop]);

				if (r.pickupTime+p.maxDelay*M2S<futureDepTime+mat[0][futureStop][r.startStop]) break; // cannot accommodate after current request - try another bus

				// if there are no requests left to iterate through, can immediately add the new one
				if (theBus.requests[j].pickupTime == 0) {
					theBus.requests[j] = r;
					accReqs++;
					printf("scheduled for %s\n", ftime(outTime, r.schTime));
					buses[theBus.id-1] = theBus;
					free(appBuses);
					return;
				}

				// next request is not null, check if the bus can pick up the new request and make it in time to next request
				futureArrTime = MAX(theBus.requests[j].pickupTime, futureDepTime+mat[0][futureStop][theBus.requests[j].startStop]);
				futureStop = theBus.requests[j].startStop;
				if (r.schTime+p.boardingTime+mat[0][r.startStop][futureStop]<=futureArrTime) {
					// can make in time before next request - update request array
					for (k = p.busCapacity-1; k>j; k--) {
						theBus.requests[k] = theBus.requests[k-1];
					}
					theBus.requests[j] = r;
					accReqs++;
					printf("scheduled for %s\n", ftime(outTime, r.schTime));
					buses[theBus.id-1] = theBus;
					free(appBuses);
					return;
				}
				// cannot make it before next request - next request is now current, repeat
			}
		}
		// request cannot be added to this bus, update array of applicable buses
		appBusCount--;
		for (j = arrPos; j<appBusCount; j++) {
			appBuses[j] = appBuses[j+1];
		}
	}
	free(appBuses);
	missedReqs++;
	printf("cannot be accommodated\n");
}

// simultaneously finds the smallest values in all the buses for next request time, bus arrival time, bus departure time, and bus boarding/disembarkation time
void closestDelays() {
	int i;
	if (requestTime<=curTime) requestTime = curTime-meanRequestDelay*log(randomize(0,1)); // queue next request
	for (i = 0; i<p.noBuses; i++) {
		int curDepTime = depTime(i, buses, mat[0]);
		if ((buses[i].nextArrival<arrivalTime)) {
			arrivalTime = buses[i].nextArrival;
			arrBusIndex = i;
		}
		if ((curDepTime<departureTime) && (curDepTime>=curTime)) {
			departureTime = curDepTime;
			depBusIndex = i;
		}
		if ((buses[i].state == ATSTOP) && (boardingTime>buses[i].nextBoardCheck)) {
			boardingTime = buses[i].nextBoardCheck;
			boardBusIndex = i;
		}
	}
}

int main(int argc, char *argv[]) {
	// set seed for randomising
	time_t t;
	srand((unsigned) time(&t));

	// parse input file
	FILE* input = fopen(argv[1], "r");
	p = parse(input);
	fclose(input);

	// initialisation steps
	curTime = 0;
	maxTime = p.stopTime*H2M*M2S;
	meanRequestDelay = (int) H2M*M2S/p.requestRate;
	outTime = (char*) malloc(12*sizeof(char)); // 11 for printed symbols + '\0'
	requestTime = 0;
	depBusIndex = -1;
	arrivalTime = INT_MAX;
	departureTime = INT_MAX;
	boardingTime = INT_MAX;
	mat = shortestPaths(p);
	buses = initBuses(p);
	initStats();

	// first steps at 0 time - new request and assign a bus to it
	r = makeRequest(p, curTime);
	assignRequest();

	// main iteration loop
	while (curTime <= maxTime) {
		closestDelays();
		closestTime = MIN(MIN(requestTime, departureTime), MIN(arrivalTime, boardingTime));
		if (closestTime == requestTime) {
			curTime = requestTime;
			r = makeRequest(p, curTime);
			assignRequest();
		} else if (closestTime == arrivalTime) {
			curTime = arrivalTime;
			buses[arrBusIndex] = arrive(buses[arrBusIndex], curTime, p.boardingTime, outTime);
			arrivalTime = INT_MAX;
		} else if (closestTime == departureTime) {
			curTime = departureTime;
			buses[depBusIndex] = depart(buses[depBusIndex], curTime, p.boardingTime, outTime, mat);
			departureTime = INT_MAX;
		} else { // if (closestTime == boardingTime)
			buses[boardBusIndex] = boarding(buses[boardBusIndex], p, curTime, boardingTime, outTime, mat);
			boardingTime = INT_MAX;
		}
	}
	// update final occupation statistics, output them alongside all other statistics
	getLastOccs(curTime, buses, p.noBuses);
	printStats();

	freeAll(p, mat, buses, outTime);
	return 1;
}

