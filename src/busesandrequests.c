#include "simulation.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>

#define M2S 60 // minutes to seconds
#define IDLE 0 // at stop, no requests, no passengers

// initialize the bus array, size specified by parameters
bus* initBuses(parameters p) {
	int i,j;
	bus* buses;
	buses = (bus*) malloc(p.noBuses*sizeof(bus));
	for (i = 0; i<p.noBuses; i++) {
		buses[i].id = i+1;
		buses[i].capacity = 0;
		buses[i].maxCapacity = p.busCapacity;
		buses[i].occChangeTime = 0;
		buses[i].curStop = 0;
		buses[i].requests = (request*) malloc(p.busCapacity*sizeof(request));
		buses[i].passengers = (request*) malloc(p.busCapacity*sizeof(request));
		for (j = 0; j<p.busCapacity; j++) {
			buses[i].requests[j].pickupTime = 0;
			buses[i].passengers[j].pickupTime = 0;
		}
		buses[i].nextArrival = INT_MAX;
		buses[i].nextBoardCheck = INT_MAX;
		buses[i].waitingStart = INT_MAX;
		buses[i].state = IDLE;
	}
	return buses;
}

// initialize a new request with random pickup time, start and end stops.
request makeRequest(parameters p, int curTime) {
	request r;
	int pickupInSecs = p.pickupInterval*M2S;
	r.pickupTime = (int) curTime-pickupInSecs*log(randomize(0,1));
	r.startStop = (int) randomize(1,p.noStops);
	r.arrTime = INT_MAX;
	r.waitTime = 0;
	do { // make sure start and end stops are not the same
		r.endStop = (int) randomize(1,p.noStops);
	} while (r.startStop == r.endStop);
	return r;
}
