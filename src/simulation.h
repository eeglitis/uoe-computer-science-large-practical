#include <stdio.h>

typedef struct parameters { // all 9 parameters match the parameters in the input file
	int busCapacity;
	int boardingTime;
	int maxDelay;
	int noBuses;
	int noStops;
	float requestRate;
	float pickupInterval;
	float stopTime;
	int** matrix;
} parameters;

typedef struct request {
	int pickupTime; // requested time for the passenger to be picked up
	int schTime; 	// scheduled time for the bus to pick up this request
	int boardTime;  // only after boarding: records the boarding time for statistical purposes
	int waitTime;	// only after boarding: records the total time the bus has waited with this passenger on board
	int arrTime; 	// only after boarding: scheduled time for the bus to drop off this passenger
	int startStop;	// the stop at which the passenger is to be picked up
	int endStop;	// the stop at which the passenger is to be dropped off
} request;

typedef struct bus {
	int id;				// uniquely identifies each bus
	int capacity;		// keeps track of the number of passengers on the bus
	int maxCapacity; 	// for ease of access - is equal to maxCapacity in the input file
	int occChangeTime; 	// time the occupancy of the bus changes, for statistical purposes
	int waitingStart; 	// time the bus starts to wait at a stop (it has more than enough time to get to the next request)
	int curStop;		// keeps track of the stop the bus is currently at, or the last one the bus left
	request* requests;	// array of pending requests for this bus. always sorted based on lowest pickupTime
	request* passengers;// array of passengers on this bus. always sorted based on lowest boardTime
	int nextArrival;	// the next time the bus is due to arrive at a stop
	int nextBoardCheck;	// only set if a boarding/disembarking event is due in parameters.boardingTime
	int state;			// the state of a bus. all four states are defined at the start of events.c
} bus;

// from parser.c
parameters parse(FILE* input);

// from helpers.c
float toFloat(char* str);
int toInt(char* str);
float randomize(int low, int high);
char* ftime(char* array, int time);
int totalSpace(bus b, int maxsize);
bus addWait(bus b, int curTime);

// from floydwarshall.c
int*** shortestPaths(parameters p);

// from busesandrequests.c
bus* initBuses(parameters p);
request makeRequest(parameters par, int curTime);

// from events.c
bus arrive(bus theBus, int curTime, int boardingTime, char* outTime);
bus depart(bus theBus, int curTime, int bTime, char* outTime, int*** mat);
int depTime(int i, bus* buses, int** paths);
bus boarding(bus theBus, parameters p, int curTime, int boardingTime, char* outTime, int*** mat);

// from simulation.c
void updateTripStats(int tripLen, int waitTime, int shortestTrip);
void updateTripEfficiency(int occ);
int updateTime(int newTime, int class);

// from postloop.c
void getLastOccs(int curTime, bus* buses, int noBuses);
void getStatistics(int avgTripDuration, float tripEfficiency, float missedReqFraction, int avgWaitTime, int avgTripDeviation);
void freeAll(parameters p, int*** mat, bus* buses, char* outTime);


