#include "simulation.h"
#include <stdio.h>
#include <stdlib.h>

#define M2S 60 // minutes to seconds

// update the occSums specified in simulation.c with the last time period before the loop exceeded maxTime
void getLastOccs(int curTime, bus* buses, int noBuses) {
	int i;
	for (i = 0; i<noBuses; i++) {
		updateTripEfficiency((curTime-buses[i].occChangeTime)*buses[i].capacity);
	}
}

// prints out the 5 required statistics in the specified format
void getStatistics(int avgTripDuration, float tripEfficiency, float missedReqFraction, int avgWaitTime, int avgTripDeviation) {
	printf("---\n");
	printf("average trip duration %d:", avgTripDuration/M2S);
	if (avgTripDuration%M2S<10) printf("0");
	printf("%d\n",avgTripDuration%M2S);
	printf("trip efficiency %.2f\n", tripEfficiency);
	printf("percentage of missed requests %.2f\n", missedReqFraction);
	printf("average passenger waiting time %d seconds\n", avgWaitTime);
	printf("average trip deviation %.2f\n", (float) avgTripDeviation/M2S);
	printf("---\n");
}

// free the memory taken up by all the arrays which were initialized using malloc
void freeAll(parameters p, int*** mat, bus* buses, char* outTime) {
	int i;
	for (i = 0; i<p.noStops; i++) {
		free(mat[0][i]);
		free(mat[1][i]);
	}
	free(mat[0]);
	free(mat[1]);
	free(mat);
	for (i = 0; i<p.noBuses; i++) {
		free(buses[i].requests);
		free(buses[i].passengers);
	}
	free(buses);
	free(outTime);
}
